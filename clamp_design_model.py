# -*- coding: utf-8 -*-
"""
Created on Sat Nov 25 15:55:14 2017

@author: reecea
"""

import numpy as np
import scipy.stats
import matplotlib.pyplot as plt
import seaborn as sns
from scipy import stats
import random
from tqdm import tqdm
import math

min_pw = 0.25
max_pw = 0.875

min_tw = 11
max_tw = 11.5

clamp_bolt_dia = 0.2

trial_settings_clamp_oc = [0.125, 0.1875, 0.25, 0.375, 0.5, 0.625, 0.75]
# apply negative impact for increased amount of positions
cost_per_clamp_pos = 0.2
cost_wgt_mult = 1.0

ITERATIONS = 10
SAMPLE_POOL_SIZE = 100000

def DefineCenters(boards):
    cx_dists_all_boards = []
    centers_all_boards = []
    
    for board in boards:
        x_dist = 0 #distance from the edge
        cx_dists = [] #center line locations from edge
        centers = []
        
        for i in range(len(board)):
            piece = board[i]
            piece_center = round((piece / 2),3)
            if i > 0:
                prev_pc_center = round((board[i-1])/2,3)
                x_dist += piece_center + prev_pc_center
            else:
                x_dist += piece_center
            
            x_dist = round(x_dist,3)
            
            cx_dists.append(x_dist)
            centers.append(piece_center)
        
        cx_dists_all_boards.append(cx_dists)
        centers_all_boards.append(centers)
    
    return cx_dists_all_boards, centers_all_boards
 
def GetDistanceBetweenCenters(cx_dists):
    all_dists = [] # all data regardless of board
    all_group_dists = [] # data by board
    
    for group in cx_dists:
        len_group = len(group)
        loop_range = len_group - 1
        group_dists = []
            
        for i in range(loop_range):
            dist = group[i+1] - group[i]
            dist = round(dist,3)
            all_dists.append(dist)
            group_dists.append(dist)
        
        all_group_dists.append(group_dists)
    
    return all_dists, all_group_dists
        

def GenerateTrialBoard(sample, min_tw, max_tw):
    board_w = 0
    pieces = []
    
    while board_w < min_tw:
        while True:
            random_choice = round(random.choice(sample),3)
            trial_w = board_w + random_choice
            if trial_w <= max_tw:
                break
        
        pieces.append(random_choice)
        board_w += random_choice
    
    return pieces


def GenerateTrialSet(sample_size, sample_dist, min_tw, max_tw, plot=False):
    boards = []
    
    for i in range(sample_size):
        board = GenerateTrialBoard(sample_dist, min_tw, max_tw) 
        boards.append(board)
    
    return boards

def BUILD_TRIAL_BOARDS(min_tw, max_tw, trial_size:int=1000, sample_size:int=10000):  
    
    distribution = scipy.stats.norm(loc=0.699, scale=0.1)
    sample_dist = distribution.rvs(size=sample_size).tolist()
    
    boards = GenerateTrialSet(trial_size, sample_dist, min_tw, max_tw)
    cx_dists, pc_centers = DefineCenters(boards)
    dist_centers_all, dist_centers_by_each = GetDistanceBetweenCenters(cx_dists)
    
    _mean = np.mean(dist_centers_all)
    _med = np.median(dist_centers_all)
    _min = min(dist_centers_all)
    _max = max(dist_centers_all)
    
    dict_boards = {'Piece_Widths':boards,
                   'Piece_Center_X_Loc':cx_dists,
                   'Piece_Center':pc_centers}
    
    dict_res = {'Trial_Dataset':sample_dist, 'Boards':boards, 'Dist_Centers_All':dist_centers_all, 'Dist_Centers_byBoard':dist_centers_by_each,
                'cx_dists':cx_dists, 'pc_centers':pc_centers, 'Board_Set':dict_boards,
                'Stats_Mean_dist_centers':_mean, 'Stats_Median_dist_centers':_med, 'Stats_Min_dist_centers':_min, 'Stats_Max_dist_centers':_max}
    return dict_res
        

def Generate_Clamp(dist_oc:float=0.7, work_length_min:float=14.0):
    '''
    dist_oc = distance on center between bolts
    work_length = distance of working length not including mounting space    
    '''
    
    x_pos = 0.0
    x_centers = []
    idx = []
    llims = []
    ulims = []
    
    lims = []
    
    _idx = 0
    
    while x_pos < work_length_min:
        x_pos += dist_oc
        x_pos = round(x_pos, 3)
        
        x_llim = x_pos - (clamp_bolt_dia / 2)
        x_llim = round(x_llim,3)
        
        x_ulim = x_pos + (clamp_bolt_dia / 2)
        x_ulim = round(x_ulim,3)
        
        idx.append(_idx)
        x_centers.append(x_pos)
        llims.append(x_llim)
        ulims.append(x_ulim)
        
        lims.append((x_llim, x_ulim))
        
        _idx += 1
    
    cost_impact = len(idx) * cost_per_clamp_pos
    positions = {'PositionId':idx, 'center_x':x_centers, 'lims':lims, 'dist_oc':dist_oc, 'work_len_min':work_length_min, 'clamp_cost':cost_impact}  
    
    return positions    

def CalcScore(clamp, _widths, _centers, _xlocs):
    pc_cnt = len(_widths)
    clamp_centers_x = clamp['center_x'].copy()
    
    scores = []
    clamp_pos = []
    
    def _calcscore(bolt_center, pc_llim, pc_ulim):
        try:
            e = math.exp(1) #eulers number
            pct = (bolt_center - pc_llim) / (pc_ulim - pc_llim)
            
            peak = (e ** (0.5 ** 4) - 1) * 1000
            
            unscaled = 0.5 - abs(pct - 0.5)
            score = ((e ** (unscaled ** 4) - 1) * 1000) / peak
            if score > 1.0:
                score = 0.0
        except:
            score = 0.0
            
        return round(score,4)
        
        
    
    for i in range(pc_cnt):
        pc_center_x = _xlocs[i]
        pc_midpoint = _centers[i]
        pc_ulim = pc_center_x + pc_midpoint
        pc_llim = pc_center_x - pc_midpoint
        
        closest_clamp_center_x = min(clamp_centers_x, key=lambda x:abs(x-pc_center_x))
        closest_clamp_center_pos = clamp_centers_x.index(closest_clamp_center_x)
        
            #clamp bolt not overlapping pieces
        if closest_clamp_center_pos not in clamp_pos:
#                check if already used
            clamp_pos_score = _calcscore(closest_clamp_center_x, pc_llim, pc_ulim)
            scores.append(clamp_pos_score)
            clamp_centers_x[i] = -1
        else:
            scores.append(0)
        
        clamp_pos.append(closest_clamp_center_pos)
 
    score = np.mean(scores)
    return {'mean_score':round(score, 4),'scores':scores,'positions':clamp_pos}

    
def RUN_TRIAL(board_set:dict, clamp):
    widths = board_set['Piece_Widths']
    centers = board_set['Piece_Center']
    x_locs = board_set['Piece_Center_X_Loc']
    
    scores = []
    
    board_cnt = len(widths)
    
    for i in tqdm(range(board_cnt), desc='Running Trial'):
        pc_sizes = widths[i]
        pc_centers = centers[i]
        pc_x_locs = x_locs[i]
        
        score = CalcScore(clamp, pc_sizes, pc_centers, pc_x_locs)
        scores.append(score)
    
#    mean_score = round(np.mean(scores['mean_score']),4)
#    print('trial mean score: ', mean_score)
    return {'oc_setting':clamp['dist_oc'], 'scores':scores, 'clamp_cost':clamp['clamp_cost']}

    
'''========================================================================='''  
_best_trials = []
for itr in range(ITERATIONS):
    print('>'*10, ' ITERATION: ', itr+1, ' of ', ITERATIONS, ' ','<'*10)
    trial_boards = BUILD_TRIAL_BOARDS(min_tw, max_tw, sample_size=SAMPLE_POOL_SIZE)
    
    trial_results = []
    current_trial = 1
    
    for clamp_oc in trial_settings_clamp_oc:
        clamp = Generate_Clamp(dist_oc=clamp_oc)
        results = RUN_TRIAL(trial_boards['Board_Set'], clamp)
        trial_results.append(results)
        print('Trial', current_trial, 'of', len(trial_settings_clamp_oc), 'complete')
        current_trial += 1
    
    
    
    '''-------------------------'''
    
    scores_master = {}
    means = []
    clamp_costs = []
    
    for trial in range(len(trial_settings_clamp_oc)):
        scores_sub_master = []
        scores_sub = trial_results[trial]['scores']
        for sub in scores_sub:
            scores_sub_master.append(sub['scores'])
        scores_sub_master_flat = [item for sublist in scores_sub_master for item in sublist]
        scores_master[trial] = scores_sub_master_flat
        means.append(np.nanmean(scores_master[trial]))
        clamp_costs.append(trial_results[trial]['clamp_cost'])
        print(np.nanmean(scores_master[trial]))
    
    
    adj_scores = []
    max_clamp_cost = max(clamp_costs)
    for item in range(len(means)):
        _mean = means[item]
        _cost = clamp_costs[item]
        _adj = ((_cost/max_clamp_cost)**2) * cost_wgt_mult
        adj_scores.append(_mean - _adj)
        print(_mean, '--->', _mean - _adj)
    
    
    
    
    max_mean_trial = adj_scores.index(max(adj_scores))
    best_trial = trial_settings_clamp_oc[max_mean_trial]
    _best_trials.append(best_trial)
    
    data = scores_master[max_mean_trial]
    
    #plt.hist(data, alpha=0.5)
    g = sns.distplot(data, fit=stats.gamma)
    
    sns.despine(top=True, left=True, right=True)
    g.set(yticklabels=[])

    overall_best_trial = stats.mode(_best_trials)[0][0]
    
    _title = 'Best Config: ' + str(overall_best_trial) + '" On Center'
    
    g.set_title(_title)
    #
    _mean = np.nanmean(data)
    _stdv = np.nanstd(data)
    
    stdv1 = _mean - _stdv
    stdv2 = _mean + _stdv
    #
    plt.axvline(x=_mean)
    plt.axvline(x=stdv1)
    plt.axvline(x=stdv2)

'''========================================================================='''
'''
incomplete idea below to graph positions of the best result.
'''
best_clamp = Generate_Clamp(overall_best_trial)

min_dist_apart_oc = 0.625

_centers = best_clamp['center_x']

for i, cx in enumerate(_centers):
    if i == 0:
        _x = cx
        _y = 0
